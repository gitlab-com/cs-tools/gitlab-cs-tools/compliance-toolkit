const path = require('path')
var webpack = require('webpack')
const publicDir = 'assets'

function publicPath () {
  const uri = process.env.APPLICATION_URL || process.env.CI_PAGES_URL || '/'

  return uri === '/' ? uri : new URL(uri).pathname
}

module.exports = {
  publicPath: publicPath(),

  outputDir: 'public',

  transpileDependencies: [
    'vuetify', '@koumoul/vjsf'
  ],

  configureWebpack: {
    plugins: [
      new webpack.EnvironmentPlugin(['CLIENT_ID', 'GITLAB_URL', 'CI_SERVER_URL', 'CI_PROJECT_URL'])
    ]
  },

  chainWebpack (config) {
    config.plugin('html').tap((args) => {
      args[0].template = path.resolve(publicDir, 'index.html')
      args[0].title = "Compliance Toolkit for GitLab"
      return args
    })
    config
      .plugin('copy')
      .use(require('copy-webpack-plugin'))
      .tap((args) => {
        return [
          [...(args[0] ? args[0] : []), { from: path.resolve(publicDir) }]
        ]
      })
  }
}
