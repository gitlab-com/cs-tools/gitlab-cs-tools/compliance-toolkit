[[_TOC_]]

# Disclaimer

This toolkit is being developed as a short-lived side project to explore capabilities meant to reduce effort and friction to adopting Compliance Frameworks within GitLab. The overall intent is to see these or similar capabilities built directly into GitLab in the future so that this toolkit becomes redundant.

This toolkit is not supported by GitLab and usage of it is at end users' own risk. Issues and MRs are welcome to report problems or contribute enhancements.

# Overview

This is a single page application, deployable to GitLab Pages, that can assist GitLab users in auditing and applying compliance frameworks across groups.

### Use cases this toolkit covers

##### As a group owner, auditor or reporter
- I want an overview of all compliance frameworks in a group and which projects have applied those frameworks

##### As a group owner
- I want to apply a default compliance framework across all projects in a group hierarchy, with the option to apply the default only on projects with no framework set
- I want to review an audit report, make changes to projects' assigned compliance frameworks, and apply those updates
- When I apply framework assignment changes to projects, I want a log of those changes and the option to undo those changes if needed

# Deployment Instructions

## Gitlab.com Users

The tooklit can be accessed directly via this project's GitLab Pages URL. In order to deploy a separate copy, follow the self-managed instructions.

## Self-managed GitLab Users

The recommended method of deployment is to GitLab Pages, as documented below, but the toolkit can be deployed as a static site to any secure endpoint.

### 1. Clone or fork this repository into a separate project

The intended users of the toolkit may need to be considered when picking a location to place the project.

If the toolkit should be accessible to all users on the instance, consider placing it in a public group.

### 2. Run a CI pipeline to deploy to GitLab Pages

This first deployment will not succeed in a working applicaton but is necessary in order to generate the Pages URL used in the next step.

Take note of the Pages URL under `Settings > Pages` following deployment

### 3. Create an Application to enable OAuth

Applications can be defined in one of the following areas:

- At the GitLab instance admin level
- Within a group
- Within a user's settings

On GitLab.com, group level is recommended, for self-managed group or admin level is recommended.

Steps to create the application:
1. Navigate to the Applications page. When defining on the group level, this is under `Settings > Applications`
2. On the Applications page, fill in the form with the following settings:
   - Name: A suitable name for the application definition, such as `Compliance Toolkit`
   - Redirect URI: The Pages URL for this project from the previous step
   - Confidential: Uncheck this setting
   - Scopes: Select `api`
3. Click `Save Application`
4. On the next page, note down the `Application ID`. This value will not be displayed again


### 4. Define a CI variable for the application client ID

In the project's CI settings, create a new protected variable `CLIENT_ID`, using the application client ID from the previous step as the value

### 5. Run another CI pipeline to complete deployment

This will configure the toolkit deployment with the appropriate application client ID.

### 6. Test authentication with GitLab

1. Navigate to the URL where the toolkit was deployed
2. Click "Login", validate a popup is displayed with a prompt to grant the tookit access to GitLab
3. Grant access. The popup should disappear and the tookit interface should be visible
4. Navigate to the Audit page of the toolkit
5. In the group search text field, enter the name of a group where the logged in user should have access. Validate a list is displayed including names of groups that match the string entered.

# Developing

This is a typsecript-based single-page web application utilizing the Vue.js and Vuetify frameworks. It accesses GitLab using the REST API via gitbeaker and the GraphQL API using Vue-Apollo. Authentication is done using the OAuth PKCE with Authorization Code flow, which is implemented fork of VueAuthenticate. Yarn is used for package management and is a prerequisite for building the code.

## Project setup

Clone the project locally and run:

```bash
yarn install
```

### To run a local hot-reloadable version of the toolkit:

1. Follow the deployment instructions for defining an application at the user setting level, and set that application's Redirect URI to `http://localhost:8080/`

2. Set the following environment variables (dotenv is recommended):
    - GITLAB_URL: Path to your GitLab instance. This is not needed if testing against GitLab.com
    - CLIENT_ID: Application ID token from the previous step

3. Run the toolkit:

    ```bash
    yarn serve
    ```

### To build a deployable version of the toolkit without using a CI pipeline:

1. Follow the deployment instructions for defining an application at the user setting level, and set that application's Redirect URI to the target endpoint where the deployment will be accessible
2. Set the following environment variables (dotenv is recommended):
    - GITLAB_URL: Path to your GitLab instance. This is not needed if testing against GitLab.com
    - APPLICATION_ID: Application ID token from the previous step
    - APPLICATION_URI: The endpoint where the toolkit will be accessible (same as Redirect URI in the previous step).
3. Build the toolkit:

    ```bash
    yarn build
    ```

4. The build artifacts will be located under the `dist` folder. Deploy these artifacts to the appropriate data store that will host the toolkit.

# Known limitations

## Notifying users of insufficient credentials
The toolkit relies on the GitLab API and GitLab's user permissions enforcement to prevent unauthorized access to groups and projects, but it does not currently check ahead of time whether an action a user is about to perform will succeed. Examples:

#### A user with read-only access to a group
  - User navigates to the Audit section of the toolkit and runs a report. This succeeds as they have the appropriate access.
  - User modifies the audit report to change projects' compliance frameworks and attempts to apply those changes. The toolkit does not know that the user has no ownership rights in the projects to make those changes, and therefore will not warn the user ahead of time that their updates will not succeed. When the user executes their changes, they will fail. The execution results will reflect this.

#### A user with mixed access to a group's projects attempts to apply a default framework to the group's projects
In this case, only projects where the user has ownership will change. The execution results will reflect this.

## API rate limiting
Attempts to audit or apply frameworks to thousands of projects in a short period may fail where rate limiting is enabled. While API rate limiting is disabled by default in self-managed instances, administrators may have enabled limits. GitLab.com also enforces API rate limits.
