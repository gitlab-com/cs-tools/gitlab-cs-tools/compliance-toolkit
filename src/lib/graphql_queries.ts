import gql from 'graphql-tag'

export function createBatch (data: any[], batchSize: number): any[] {
  const numBatches = Math.ceil(data.length / batchSize)
  const batches = []

  for (let i = 0; i < numBatches; i++) {
    batches.push(data.slice(i * batchSize, (i + 1) * batchSize))
  }

  return batches
}

export const GET_GROUP_COMPLIANCE_FRAMEWORKS = gql`
  query GET_GROUP_COMPLIANCE_FRAMEWORKS($fullPath: ID!) {
    group(fullPath: $fullPath) {
      name
      id
      complianceFrameworks {
        nodes {
          id
          name
          pipelineConfigurationFullPath
          color
          description
          id
        }
      }
    }
  }
  `

export const GET_PROJECT_AUDIT_DETAILS = gql`
  query GET_PROJECT_AUDIT_DETAILS($fullPath: ID!) {
    project(fullPath: $fullPath) {
      id
      name
      path
      webUrl
      complianceFrameworks {
        nodes {
          id
          name
        }
      }
      scanExecutionPolicies {
        nodes {
          name
          enabled
          description
        }
      }
    }
  }
  `

export const GET_BATCHED_PROJECT_AUDIT_DETAILS = gql`
  query GET_BATCHED_PROJECT_AUDIT_DETAILS($ids: [ID!]) {
    projects(ids: $ids) {
      nodes {
        id
        name
        path
        fullPath
        webUrl
        complianceFrameworks {
          nodes {
            id
            name
            color
            pipelineConfigurationFullPath
          }
        }
        scanExecutionPolicies {
          nodes {
            name
            enabled
            description
          }
        }
      }
    }
  }
  `
