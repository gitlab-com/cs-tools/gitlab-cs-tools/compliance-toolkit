import gql from 'graphql-tag'

export const APPLY_COMPLIANCE_FRAMEWORK = gql`
  mutation APPLY_COMPLIANCE_FRAMEWORK($projectId: ProjectID!, $frameworkId: ComplianceManagementFrameworkID) {
    projectSetComplianceFramework(input: {projectId: $projectId, complianceFrameworkId: $frameworkId}) {
      clientMutationId
      errors
      project{
        id
        complianceFrameworks {
          nodes {
            id
            name
            color
          }
        }
      }
    }
  }
  `
