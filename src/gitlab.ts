import { Gitlab } from '@gitbeaker/browser'
import store from './store/index'

export function getRestAPI (): InstanceType<typeof Gitlab> {
  return new Gitlab({ oauthToken: store.state.token, host: getHost() })
}

export function getHost (): string {
  return (process.env.GITLAB_URL || process.env.CI_SERVER_URL || 'https://gitlab.com').replace(/\/$/, '')
}

export function getProjectURL (): string {
  return process.env.CI_PROJECT_URL || ''
}

export function getGraphQLEndpoint (): string {
  return getHost() + '/api/graphql'
}
