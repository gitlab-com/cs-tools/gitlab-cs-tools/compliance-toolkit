import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import CopyFrameworks from '../views/CopyFrameworks.vue'
import ApplyDefaultFrameworks from '../views/ApplyDefaultFrameworks.vue'
import ImportProjectAssignments from '../views/ImportProjectAssignments.vue'
import Audit from '../views/Audit.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Audit Framework Assignments',
    component: Audit
  },
  {
    path: '/audit',
    name: 'Audit Framework Assignments',
    component: Audit
  },
  {
    path: '/copy',
    name: 'Copy Framework Definitions Across Groups',
    component: CopyFrameworks
  },
  {
    path: '/import',
    name: 'Import Project Framework Assignments',
    component: ImportProjectAssignments
  },

  {
    path: '/apply',
    name: 'Apply a Framework to Projects in a Group',
    component: ApplyDefaultFrameworks
  }
]

const router = new VueRouter({
  routes
})

export default router
