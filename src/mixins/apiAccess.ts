import { Component, Vue } from 'vue-property-decorator'
import { Gitlab } from '@gitbeaker/browser'
import * as Queries from '../lib/graphql_queries'
@Component
export default class ApiAccess extends Vue {
  SAAS_URL = 'https://gitlab.com'
  cachedGroups: any = {}
  cachedFrameworks: any = {}
  cachedProjects: any[] = []

  getRestAPI (): InstanceType<typeof Gitlab> {
    return new Gitlab({ oauthToken: this.$auth.getToken(), host: this.getHost() })
  }

  getHost (): string {
    return (process.env.GITLAB_URL || this.SAAS_URL).replace(/\/$/, '')
  }

  isSaaS (): boolean {
    return this.getHost() === this.SAAS_URL
  }

  async findTopLevelGroupForProject (project: any): Promise<any> {
    if (project.namespace && project.namespace.kind === 'group') {
      // eslint-disable-next-line security/detect-object-injection
      let group = this.cachedGroups[project.namespace.id]
      if (!group) {
        const response = await this.getRestAPI().Groups.show(project.namespace.id)
        if (Array.isArray(response) && response.length === 1) {
          group = response[0]
        } else {
          group = response
        }
      }
      return this.findTopLevelGroup(group)
    }
  }

  async findTopLevelGroup (group: any): Promise<any> {
    if (group == null || group.parent_id == null) {
      return group
    } else {
      let parent = this.cachedGroups[group.parent_id]
      if (!parent) {
        parent = await this.getRestAPI().Groups.show(group.parent_id)
        this.cachedGroups[group.parent_id] = parent
      }
      return await this.findTopLevelGroup(parent)
    }
  }

  async getFrameworksForProject (project: any): Promise<any> {
    const topLevelGroup = await this.findTopLevelGroupForProject(project)
    if (!this.cachedFrameworks[topLevelGroup.id]) {
      await this.$apollo.query({
        query: Queries.GET_GROUP_COMPLIANCE_FRAMEWORKS,
        variables: {
          fullPath: topLevelGroup.full_path ?? null
        }
      }).then((result: any) => {
        this.cachedFrameworks[topLevelGroup.id] = result.data.group.complianceFrameworks.nodes
      })
    }

    return this.cachedFrameworks[topLevelGroup.id]
  }

  async getProjectsInGroup (group: any, includeSubGroups: boolean): Promise<any> {
    return await this.getRestAPI().Groups.projects(group.id, { include_subgroups: includeSubGroups, with_shared: false })
  }

  async getProjectbyID (id: number): Promise<any> {
    let project = this.cachedProjects.find((entry) => entry.id === id)
    if (!project) {
      const response = await this.getRestAPI().Projects.show(id)
      if (Array.isArray(response) && response.length === 1) {
        project = response[0]
      } else {
        project = response
      }
      this.cachedProjects.push(project)
    }
    return project
  }

  async getProjectsbyID (projectIDs: number[]): Promise<any> {
    return projectIDs.map(async (id: number) => {
      await this.getProjectbyID(id)
    })
  }

  updateProjectCache (projectId: number, update: any): void {
    const index = this.cachedProjects.findIndex((entry: any) => entry.id === projectId)
    if (index !== -1) {
      // eslint-disable-next-line security/detect-object-injection
      this.cachedProjects[index] = Object.assign(this.cachedProjects[index], update)
    }
  }
}
