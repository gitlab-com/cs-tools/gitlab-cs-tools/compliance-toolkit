export interface State {
    token: string,
    refreshToken: '',
    nextRefresh: Date,
    user: string,
    authInProgress: boolean
    validForm: boolean
}

export interface AuditState {
    group: Record<string, string>
}

export interface Getters {
    isAuthenticated: boolean,
    isAdmin: boolean,
    loading: boolean
}

export type GettersDefinition = {
    [P in keyof Getters]: (state: State, getters: Getters) => Getters[P];
}
