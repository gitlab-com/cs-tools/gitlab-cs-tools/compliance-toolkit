import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import ApiAccess from '@/mixins/apiAccess'

import axios from 'axios'
import { State, GettersDefinition } from '../types/store'
import createPersistedState from 'vuex-persistedstate'
import SecureLS from 'secure-ls'
import log from 'loglevel'

const ls = new SecureLS({ isCompression: false })

Vue.use(Vuex)
Vue.use(VueAxios, axios)

const state: State = {
  token: '',
  refreshToken: '',
  nextRefresh: new Date(),
  user: '',
  authInProgress: false,
  validForm: true
}

const getters: GettersDefinition = {
  isAuthenticated: (state: { token: string }) => state.token !== '',
  isAdmin: (state: {user: any}) => state.user?.is_admin || false,
  loading: (state: { authInProgress: boolean }) => state.authInProgress
}

export default new Vuex.Store({
  state: state,
  getters: getters,
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setAuthInProgress (state, progress) {
      state.authInProgress = progress
    },
    setToken (state, token) {
      state.token = token
    },
    setRefreshToken (state, token) {
      state.refreshToken = token
    }
  },
  actions: {
    resetState () {
      this.dispatch('audit/resetState', { root: true })
    },
    setUser ({ commit }, user) {
      commit('setUser', user)
    },
    authenticate ({ commit }, vue: ApiAccess) {
      commit('setAuthInProgress', true)
      vue.$auth.authenticate('gitlab', {}).then(() => {
        commit('setAuthInProgress', false)
        commit('setToken', vue.$auth.getToken())
        const api = vue.getRestAPI()
        api.Users.current().then((user:any) => {
          log.debug('API user: ' + user)
          commit('setUser', user)
        })
      })
    },
    refresh ({ commit }, vue: any) {
      if (this.getters.isAuthenticated) {
        vue.$auth.refresh().then(() => {
          commit('setToken', vue.$auth.getToken())
        }).catch(() => {
          this.dispatch('authenticate', vue)
        })
      }
    },
    logout ({ commit }, vue: any) {
      vue.$auth.logout()
      commit('setToken', '')
    }
  },
  plugins: [
    createPersistedState({
      paths: ['token', 'user'],
      storage: {
        getItem: (key) => ls.get(key),
        setItem: (key, value) => ls.set(key, value),
        removeItem: (key) => ls.remove(key)
      }
    })
  ]
})
