import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    themes: {
      light: {
        primary: '#292961',
        secondary: '#fc6d26',
        accent: '#e24329'
      },
      dark: {
        primary: '#292961',
        secondary: '#fc6d26',
        accent: '#e24329'
      }
    }
  }
})
