import { ApolloClient, InMemoryCache } from '@apollo/client/core'
import { BatchHttpLink } from '@apollo/client/link/batch-http'
import { ApolloLink, concat } from '@apollo/client/link/core'
import * as GitLab from '../gitlab'
import store from '../store'

const httpLink = new BatchHttpLink({
  uri: GitLab.getGraphQLEndpoint(),
  batchMax: 20,
  batchInterval: 50,
  batchDebounce: true
})

const authentication = new ApolloLink((operation, forward) => {
  const token = store.state.token
  operation.setContext({
    headers: {
      authorization: token ? `Bearer ${token}` : ''
    }
  })
  return forward(operation)
})

export const apolloClient = new ApolloClient({
  link: concat(authentication, httpLink),
  cache: new InMemoryCache()
})
