import Vue from 'vue'
import VueAuthenticate from '@simbachain/vue-authenticate'
import * as GitLabHelper from '../gitlab'

const baseUrl = GitLabHelper.getHost().replace(/\/?$/, '/')
const authUrl = baseUrl + 'oauth/authorize'
const tokenUrl = baseUrl + 'oauth/token'
const redirectUrl = window.location.origin + window.location.pathname

declare module 'vue/types/vue' {
  interface Vue {
    $auth: any
  }
}

Vue.use(VueAuthenticate, {
  baseUrl: '',
  refreshGrantType: 'refresh_token',
  refreshType: 'storage',
  refreshTokenName: 'refresh_token',
  providers: {
    gitlab: {
      name: 'GitLab',
      url: tokenUrl,
      clientId: process.env.CLIENT_ID,
      redirectUri: redirectUrl,
      authorizationEndpoint: authUrl,
      oauthType: '2.0',
      pkce: 'S256',
      popupOptions: { width: 600, height: 650 },
      refreshGrantType: 'refresh_token',
      refreshType: 'storage',
      responseParams: {
        clientId: 'client_id',
        redirectUri: 'redirect_uri'
      },
      refreshParams: {
        clientId: 'client_id',
        grantType: 'grant_type'
      }
    }
  }
})
