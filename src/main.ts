import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import './plugins/auth'
import VueAxios from 'vue-axios'
import axios from 'axios'
import VueApollo from 'vue-apollo'
import { apolloClient } from './plugins/apollo'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(VueApollo)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

new Vue({
  router,
  store,
  vuetify,
  apolloProvider,
  render: h => h(App)
}).$mount('#app')
